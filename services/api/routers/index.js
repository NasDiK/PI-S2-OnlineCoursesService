const usersRouter = require('./users');
const authRouter = require('./authRouter');

module.exports = {
  usersRouter,
  authRouter
};